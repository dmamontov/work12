#!/usr/bin/env bash

admin_var=$(grep "admin" /etc/group | grep -v "printadmin")

vacations="Jan01 Jan02 Jan03 Jan04 Jan05 Jan06 Jan07 Jan08 Feb23 Mar08 May01 May09 June12 Now04"

#check weekends
if [[ $(date +%u) -gt 5 ]] || [[ $vacations == *"$(date +"%b%d")"* ]] ; then

        if echo $admin_var | grep $PAM_USER; then
               echo "check admin"
               exit 0
        fi

        echo "chech weekends"
        exit 1
fi
